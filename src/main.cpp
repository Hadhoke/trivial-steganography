// -*- C++ -*-
/*
     _               _ _           _        
    | |             | | |         | |       
    | |__   __ _  __| | |__   ___ | | _____ 
    | '_ \ / _` |/ _` | '_ \ / _ \| |/ / _ \
    | | | | (_| | (_| | | | | (_) |   <  __/
    |_| |_|\__,_|\__,_|_| |_|\___/|_|\_\___|

  main.cpp - This file is part of the Trivial Steganography Project

*/

#include <iostream>

#include "CImg.h"

#include "operator.h"
  
/**
 * Affiche une documentation d'utilisation
 */
void show_doc() {
  std::cout << "=== Trivial Steganography ===" << std::endl
            << "Usage: " << std::endl << std::endl
            << "  trivial-steganography  hide  <image_name>  <image_name_to_hide>  <output_image_name>" << std::endl
            << "    Cache une image dans une autre, et sauvegarde l'image resultat" << std::endl << std::endl
            << "  trivial-steganography  extract  <image_name>  <output_image_name>" << std::endl
            << "    Extrait une image dissimulé dans une autre image, et sauvegarde l'image extraite" << std::endl;
}

int main(int argc, const char* argv[]) {
  if (argc != 4 && argc != 5) {
    show_doc();
    return 0;
  }

  if (std::string(argv[1]) == "hide") {
    hide_image(argv[2], argv[3], argv[4]);
  } else if (std::string(argv[1]) == "extract") {
    extract_image(argv[2], argv[3]);
  } else {
    show_doc();
  }
  
  return 0;
}

