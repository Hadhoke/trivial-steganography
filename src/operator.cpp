// -*- C++ -*-
/*
     _               _ _           _        
    | |             | | |         | |       
    | |__   __ _  __| | |__   ___ | | _____ 
    | '_ \ / _` |/ _` | '_ \ / _ \| |/ / _ \
    | | | | (_| | (_| | | | | (_) |   <  __/
    |_| |_|\__,_|\__,_|_| |_|\___/|_|\_\___|

  operator.cpp - This file is part of the Trivial Steganography Project

*/

#include <iostream>
#include <queue>

#include "operator.h"


void hide_image(const std::string& input_path_1, const std::string& input_path_2, const std::string& output_path) {
  ImageUchar image_1(input_path_1.c_str());
  ImageUchar image_2(input_path_2.c_str());

  rgb_to_grayscale(image_2);

  hide(image_2, image_1);

  std::cout << "Result image save into " << output_path << std::endl;
  image_1.save_bmp(output_path.c_str());
}

void extract_image(const std::string& input_path, const std::string& output_path) {
  ImageUchar image_source(input_path.c_str());
  ImageUchar image_extracted(image_source.width(), image_source.height());

  extract(image_source, image_extracted);

  std::cout << "Extracted image save into " << output_path << std::endl;
  image_extracted.save_bmp(output_path.c_str());
}


/**
 * Transforme une image couleur en noir et blanc
 *
 * @param image [in, out] Image à transformer
 */
void rgb_to_grayscale(ImageUchar& image) {
  for (int y = 0; y < image.height(); ++y) {
    for (int x = 0; x < image.width(); ++x) {
      image(x, y, 0) = uint8_t(0.212671f * image(x, y, 0) + // Rouge
                               0.715160f * image(x, y, 1) + // Vert
                               0.072169f * image(x, y, 2)); // Bleu
    }
  }
  image.resize(-100, -100, -100, 1); // Change le nombre de canaux de l'image,
                                     // de 3 (rouge, vert, bleu) vers 1 (niveau de gris)
                                     // -100 indique ici 100%, et n'affecte donc 
                                     // pas la largeur, la hauteur et la profondeur de l'image
}


/**
 * Dissimuler une image en niveau de gris (image_to_hide) dans une image couleur (image_couleur).
 *
 * Soit un pixel P de image_couleur composé de trois octets, R pour le rouge, V pour le vert, B pour le bleu
 *   rouge = R0 R1 R2 R3 R4 R5 R6 R7 -> 8 bits contenant le niveau de rouge
 *   vert  = V0 V1 V2 V3 V4 V5 V6 V7 -> 8 bits contenant le niveau de vert
 *   bleu  = B0 B1 B2 B3 B4 B5 B6 B7 -> 8 bits contenant le niveau de bleu
 * 
 * et un pixel Pg de image_to_hide composé d'un octet contenant la nuance de gris
 *   gris  = G0 G1 G2 G3 G4 G5 G6 G7 -> 8 bits contenant le niveau de gris
 *
 * alors nous allons remplacer certains bits de P de la façon suivante pour dissimuler Pg dans P :
 *   rouge = R0 R1 R2 R3 R4 G0 G1 G2
 *   vert  = V0 V1 V2 V3 V4 V5 G3 G4
 *   bleu  = B0 B1 B2 B3 B4 G5 G6 G7
 *
 * @param image_to_hide [in]      Image en niveau de gris à dissimuler
 * @param image_couleur [in, out] Image original
 */
void hide(const ImageUchar &image_to_hide, ImageUchar &image_couleur) {

  // Double boucle (une pour la hauteur, une pour la largeur) permetant de parcourir tous les pixels de image_couleur
  for (int y = 0; y < image_couleur.height(); ++y) {
    for (int x = 0; x < image_couleur.width(); ++x) {

      // La première étape consiste à découper le pixel de image_to_hide
      // (contenu sur un octet car c'est une image en noir et blanc)
      // en trois parties : la première doit faire trois bits, la deuxième deux bits et la troisième trois bits.
      // Soit 3 + 2 + 3 = 8 bits = 1 octet

      // Initialisation des trois variables contenant chacune une partie du pixel
      uint8_t part_1 = 0;
      uint8_t part_2 = 0;
      uint8_t part_3 = 0;
      
      // Vérifie que image_to_hide contient un pixel aux coordonnées (x, y)
      // Cela évite d'accéder à un pixel qui se trouve en dehors de l'image
      if (image_to_hide.containsXYZC(x, y)) {
        // image_to_hide(x, y) -> valeur du pixel au coordonnées (x, y) contenu sur un octet (8 bits)
        //
        // Récupération des trois premiers bits de l'octet dans part_1
        //
        // image_to_hide(x, y)              -> xxxx xxxx (x vaut 0 ou 1)
        // image_to_hide(x, y) & 224        -> xxx0 0000
        // (image_to_hide(x, y) & 224) >> 5 -> 0000 0xxx
        part_1 = (image_to_hide(x, y) & 224) >> 5;

        // Récupération des deux bits suivant dans part_2
        //
        // image_to_hide(x, y)              -> xxxx xxxx (x vaut 0 ou 1)
        // image_to_hide(x, y) & 24         -> 000x x000
        // (image_to_hide(x, y) & 24) >> 3  -> 0000 00xx
        part_2 = (image_to_hide(x, y) &  24) >> 3;

        // Récupération des trois derniers bits dans part_3
        //
        // image_to_hide(x, y)              -> xxxx xxxx (x vaut 0 ou 1)
        // image_to_hide(x, y) & 24         -> 0000 0xxx
        part_3 =  image_to_hide(x, y) &   7;
      }


      // Remplissage des trois derniers bits de l'octet contenant le rouge à 0 (xxxx x000) (x vaut 0 ou 1)
      image_couleur(x, y, 0) &= 248; // rouge & 1111 1000

      // Remplissage des deux derniers bits de l'octet contenant le vert à 0 (xxxx xx00)
      image_couleur(x, y, 1) &= 252; // vert & 1111 1100

      // Remplissage des trois derniers bits de l'octet contenant le bleu à 0 (xxxx x000)
      image_couleur(x, y, 2) &= 248; // bleu & 1111 1000

      // Il ne reste plus qu'à insérer part_1, part_2 et part_3 dans image_couleur, 
      // une dans chaque composante (couleur).
      // L'astuce pour que la modification de l'image ne soit pas visible (ou le moins possible) est
      // de modifier les bits de poids faibles,
      // ceux les plus à droite. Cela permet de faire varier la valeur de chaque couleur assez faiblement 
      // pour ne pas être visible à l'oeil nu.
      image_couleur(x, y, 0) = image_couleur(x, y, 0) | part_1; // xxxx x000 | 0000 0xxx
      image_couleur(x, y, 1) = image_couleur(x, y, 1) | part_2; // xxxx xx00 | 0000 00xx
      image_couleur(x, y, 2) = image_couleur(x, y, 2) | part_3; // xxxx x000 | 0000 0xxx
    }
  }
}


/**
 * Extraire l'image en niveau de gris dissimulée dans une image
 * Il faut appliquer l'algorithme inverse pour retrouver l'image cachée.
 *
 * @param image           [in]  Image stéganographiée
 * @param image_extracted [out] Image en niveau de gris
 */
void extract(const ImageUchar &image, ImageUchar &image_extracted) {
  for (int y = 0; y < image.height(); ++y) {
    for (int x = 0; x < image.width(); ++x) {
      // Récupération des trois derniers bits contenus dans le rouge
      uint8_t part_1 = (image(x, y, 0) & 7) << 5; //0000 0111

      // Récupération des deux derniers bits contenus dans le vert
      uint8_t part_2 = (image(x, y, 1) & 3) << 3; //0000 0011

      // Récupération des trois derniers bits contenus dans le bleu
      uint8_t part_3 =  image(x, y, 2) & 7;       //0000 0111

      // Fusion de part_1, part_2 et part_3 afin de recomposer le niveau de gris original
      image_extracted(x, y) = (part_1 | part_2) | part_3;
    }
  }
}