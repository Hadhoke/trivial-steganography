// -*- C++ -*-
/*
     _               _ _           _        
    | |             | | |         | |       
    | |__   __ _  __| | |__   ___ | | _____ 
    | '_ \ / _` |/ _` | '_ \ / _ \| |/ / _ \
    | | | | (_| | (_| | | | | (_) |   <  __/
    |_| |_|\__,_|\__,_|_| |_|\___/|_|\_\___|

  operator.h - This file is part of the Trivial Steganography Project

*/

#ifndef LabyrinthSolver2_operator_h
#define LabyrinthSolver2_operator_h

// Uncomment to use png input image
// (don't forget to uncomment last line of CMakeLists.txt to add libpng and libjpeg)
// #define cimg_use_png 1
  
#include "CImg.h"

typedef cimg_library::CImg<uint8_t> ImageUchar;

void hide_image(const std::string& input_path_1, const std::string& input_path_2, const std::string& output_path);
void extract_image(const std::string& input_path, const std::string& output_path);

void rgb_to_grayscale(ImageUchar& image);
void hide(const ImageUchar &imc, ImageUchar &img);
void extract(const ImageUchar &ims, ImageUchar &imd);

#endif
